<?php
session_start();
if( isset($_SESSION['token'])==false)
{
    $_SESSION['token'] = bin2hex(random_bytes(32)); //Token CSRF
}
if(isset($_SESSION['role']))
{
    $role=$_SESSION['role'];
}

require("./controller/controller.php");
    try{
        if(isset($_GET['action']))
        {
            $action=test_input($_GET['action']);
           
                //ROUTES DE L'ADMIN
 
                   
                    if($action=="pageAdmin"&&isset($role))
                    {

                        if($role==="Admin")
                        {
                        require ("./controller/adminController.php");
                        routePageAdmin();
                        }
                   
                    }
                
                
                //FIN DES ROUTES DE l'ADMIN

                //ROUTES DES EDITEURS

                   
                    else if ($action=="pageEditeur"&&isset($role))
                    {
                      
                            if($role==="Editeur"||$role==="Admin")
                            {
                            require("./controller/editeurController.php");
                            routePageEditeur();
                            }
                    }
                    

                //FIN ROUTES DES EDITEUR
            
            // ROUTES DES VISITEURS (SANS COMPTES)
            
                elseif($action==="formulaireLogin")
                {
                    routeFormulaireLogin();
                }
                elseif ($action=="login"&&isset($_POST['email'])&&isset($_POST['mdp']))
                {
                    $email=test_input($_POST['email']);
                    $mdp=sha1(test_input($_POST['mdp']));
                    routeLogin($email,$mdp);
                }
                else{
                    //Page 404
                    //ou
                    routeFormulaireLogin();
                }
            
            //FIN ROUTES DES VISITEURS
         
            }
        else{
            routeHome();
        }

    }
    catch (Exception $e) {
        echo 'Erreur : ' . $e->getMessage();
    }
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>