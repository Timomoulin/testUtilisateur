<?php $title = 'Login'; ?>

<?php ob_start(); ?>

<div class="container d-flex flex-column col-md-8 col-lg-6 p-3">
<h1 class="align-self-center">Login</h1>
    <?php 
    if(isset($_GET["resultat"]))
    {
        $resultat=test_input($_GET["resultat"]);
        if($resultat==0)
        {
            echo ('<h2 class="text-danger"> Echec </h2>');
        }
    } 
    ?>
<form action="./?action=login&amp;" method="post">
<input type="hidden" name="token" value="<?=$_SESSION['token']?>"/>
<label for="inputEmail"">Email :</label>
<input type="email" id="inputEmail" name="email" required class="form-control my-2">
<label for="">MDP :</label>
<input type="password" id="inputMDP" name="mdp" size="6" required class="form-control my-2">
<button class="btn btn-primary my-2 ">Envoyer</button>
</form>
<br>
</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>