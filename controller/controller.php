<?php
require ("./model/bdd.php");
function routeHome(){
    require("./view/home.php");
}
function routeFormulaireLogin(){
    if(isset($_SESSION['id']))
    {
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
            header("location:./");
        }
    }
    require("./view/login.php");
}
function routeLogin($email,$mdp)
{
    if ($_POST['token'] == $_SESSION['token']) { 
        $userManager= new UtilisateurManager();
        echo("token ok");
        $resultat =$userManager->verificationLogin($email,$mdp);
        if( $resultat ==false)
        {
            header("Location:./?action=formulaireLogin&resultat=0");
            die;
        }
        else {
            $_SESSION['role']=$resultat->getRole();
            $_SESSION['email']=$resultat->getEmail();
            $_SESSION['id']=$resultat->getNumUtilisateur();
            header("Location:./");
        }
    }
}
?>