<?php

class UtilisateurManager extends BDD
{
    function __construct()
    {
        parent::__construct();
    }

    function verificationLogin($email,$mdp)
    {
        try {
            $sql = $this->connex->prepare("SELECT * FROM utilisateur where Email=:email and Mdp=:mdp");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Utilisateur');
            $sql->bindParam(":email", $email);
            $sql->bindParam(":mdp", $mdp);
            $sql->execute();
            $resultat = ($sql->fetchAll());
            if(count($resultat)>=1)
            {
                return $resultat[0];
            }
            else{
                return false;
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}