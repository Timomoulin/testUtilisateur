<?php
require("managers/UtilisateurManager.php");
require("classes/Utilisateur.class.php");
class BDD
{
    public $connex;

    function __construct()
    {
        $this->connex = new PDO("mysql:host=localhost;dbname=testutilisateur", "root", "", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $this->connex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
?>